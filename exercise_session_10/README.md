# Exercise 10#

**Exercise 1**

RUN1  
srun: job 31323866 queued and waiting for resources  
srun: job 31323866 has been allocated resources  
PI = 3.141592653589793 computed in 0.1163 seconds  
PI = 3.141592653589793 computed in 0.01426 seconds  
PI = 3.141592653589793 computed in 0.01423 seconds  
PI = 3.141592653589793 computed in 0.01407 seconds  
PI = 3.141592653589793 computed in 0.01398 seconds  

RUN2  
srun: job 31324033 queued and waiting for resources  
srun: job 31324033 has been allocated resources  
PI = 3.141592653589793 computed in 0.14 seconds  
PI = 3.141592653589793 computed in 0.01425 seconds  
PI = 3.141592653589793 computed in 0.01422 seconds  
PI = 3.141592653589793 computed in 0.01412 seconds  
PI = 3.141592653589793 computed in 0.01403 seconds  

The first iteration takes always 10 times more, because of overhead due to GPU set-up procedures.    

**Exercise 2**

RUN1  
srun: job 31324549 queued and waiting for resources  
srun: job 31324549 has been allocated resources  
PI = 3.14159265358979 computed in 0.08734 seconds  

RUN2  
srun: job 31324592 queued and waiting for resources  
srun: job 31324592 has been allocated resources  
PI = 3.14159265358979 computed in 0.08616 seconds  

The cuda code with configuration N_BLOCK = 112 and N_THREADS = 16 was 7 times slower, because configuration does not match hardware.   

**Exercise 3**

Result of CUDA code with different configuration sorted by timings.  

srun: job 31325406 queued and waiting for resources  
srun: job 31325406 has been allocated resources  
NUM_BLOCK NUM_THREAD **300 64    PI = 3.141592653589796 computed in 0.008497 seconds**  
NUM_BLOCK NUM_THREAD 600 32    PI = 3.141592653589796 computed in 0.008529 seconds  
NUM_BLOCK NUM_THREAD 600 64    PI = 3.141592653589797 computed in 0.008555 seconds  
NUM_BLOCK NUM_THREAD 420 64    PI = 3.141592653589791 computed in 0.00873 seconds  
NUM_BLOCK NUM_THREAD 600 128   PI = 3.141592653589801 computed in 0.008838 seconds  
NUM_BLOCK NUM_THREAD 420 96    PI = 3.141592653589794 computed in 0.008913 seconds  
NUM_BLOCK NUM_THREAD 360 64    PI = 3.141592653589796 computed in 0.008935 seconds  
NUM_BLOCK NUM_THREAD 600 96    PI = 3.141592653589796 computed in 0.008989 seconds  
NUM_BLOCK NUM_THREAD 420 128   PI = 3.14159265358979 computed in 0.009055 seconds  
NUM_BLOCK NUM_THREAD 600 160   PI = 3.141592653589786 computed in 0.009092 seconds  
NUM_BLOCK NUM_THREAD 420 160   PI = 3.141592653589807 computed in 0.009107 seconds  
NUM_BLOCK NUM_THREAD 360 128   PI = 3.141592653589793 computed in 0.009163 seconds  
NUM_BLOCK NUM_THREAD 300 96    PI = 3.141592653589794 computed in 0.009218 seconds  
NUM_BLOCK NUM_THREAD 300 128   PI = 3.141592653589797 computed in 0.00936 seconds  
NUM_BLOCK NUM_THREAD 360 96    PI = 3.141592653589796 computed in 0.009432 seconds  
NUM_BLOCK NUM_THREAD 300 160   PI = 3.141592653589788 computed in 0.009435 seconds  
NUM_BLOCK NUM_THREAD 180 96    PI = 3.141592653589794 computed in 0.009438 seconds  
NUM_BLOCK NUM_THREAD 360 160   PI = 3.141592653589796 computed in 0.009492 seconds  
NUM_BLOCK NUM_THREAD 240 128   PI = 3.141592653589798 computed in 0.009687 seconds  
NUM_BLOCK NUM_THREAD 600 112   PI = 3.141592653589807 computed in 0.009936 seconds  
NUM_BLOCK NUM_THREAD 600 144   PI = 3.1415926535898 computed in 0.009956 seconds  
NUM_BLOCK NUM_THREAD 240 96    PI = 3.141592653589796 computed in 0.01006 seconds  
NUM_BLOCK NUM_THREAD 420 144   PI = 3.141592653589798 computed in 0.01007 seconds  
NUM_BLOCK NUM_THREAD 180 128   PI = 3.141592653589796 computed in 0.01012 seconds  
NUM_BLOCK NUM_THREAD 240 160   PI = 3.141592653589797 computed in 0.01012 seconds  
NUM_BLOCK NUM_THREAD 240 64    PI = 3.141592653589795 computed in 0.01013 seconds  
NUM_BLOCK NUM_THREAD 420 112   PI = 3.141592653589751 computed in 0.01019 seconds  
NUM_BLOCK NUM_THREAD 360 112   PI = 3.141592653589794 computed in 0.01035 seconds  
NUM_BLOCK NUM_THREAD 180 160   PI = 3.141592653589794 computed in 0.01042 seconds  
NUM_BLOCK NUM_THREAD 360 144   PI = 3.141592653589798 computed in 0.01043 seconds  
NUM_BLOCK NUM_THREAD 300 144   PI = 3.141592653589798 computed in 0.01044 seconds  
NUM_BLOCK NUM_THREAD 600 80    PI = 3.141592653589788 computed in 0.01058 seconds  
NUM_BLOCK NUM_THREAD 120 128   PI = 3.141592653589795 computed in 0.01059 seconds  
NUM_BLOCK NUM_THREAD 420 80    PI = 3.141592653589792 computed in 0.0106 seconds  
NUM_BLOCK NUM_THREAD 300 112   PI = 3.141592653589792 computed in 0.01062 seconds  
NUM_BLOCK NUM_THREAD 300 80    PI = 3.141592653589791 computed in 0.01098 seconds  
NUM_BLOCK NUM_THREAD 240 112   PI = 3.141592653589791 computed in 0.01102 seconds  
NUM_BLOCK NUM_THREAD 240 144   PI = 3.141592653589796 computed in 0.01117 seconds  
NUM_BLOCK NUM_THREAD 420 32    PI = 3.141592653589794 computed in 0.01119 seconds  
NUM_BLOCK NUM_THREAD 360 80    PI = 3.141592653589794 computed in 0.01122 seconds  
NUM_BLOCK NUM_THREAD 300 48    PI = 3.141592653589795 computed in 0.01123 seconds  
NUM_BLOCK NUM_THREAD 600 48    PI = 3.141592653589794 computed in 0.01123 seconds  
NUM_BLOCK NUM_THREAD 180 80    PI = 3.141592653589795 computed in 0.01128 seconds  
NUM_BLOCK NUM_THREAD 180 112   PI = 3.141592653589796 computed in 0.01149 seconds  
NUM_BLOCK NUM_THREAD 180 144   PI = 3.141592653589803 computed in 0.0115 seconds  
NUM_BLOCK NUM_THREAD 420 48    PI = 3.141592653589796 computed in 0.01152 seconds  
NUM_BLOCK NUM_THREAD 360 48    PI = 3.141592653589794 computed in 0.01176 seconds  
NUM_BLOCK NUM_THREAD 240 80    PI = 3.141592653589796 computed in 0.012 seconds  
NUM_BLOCK NUM_THREAD 120 160   PI = 3.141592653589796 computed in 0.01205 seconds  
NUM_BLOCK NUM_THREAD 120 112   PI = 3.141592653589794 computed in 0.01207 seconds  
NUM_BLOCK NUM_THREAD 180 64    PI = 3.141592653589788 computed in 0.01296 seconds  
NUM_BLOCK NUM_THREAD 360 32    PI = 3.141592653589788 computed in 0.01296 seconds  
NUM_BLOCK NUM_THREAD 120 144   PI = 3.141592653589794 computed in 0.01332 seconds  
NUM_BLOCK NUM_THREAD 240 48    PI = 3.141592653589788 computed in 0.01344 seconds  
NUM_BLOCK NUM_THREAD 120 96    PI = 3.141592653589788 computed in 0.01349 seconds  
NUM_BLOCK NUM_THREAD 300 32    PI = 3.141592653589789 computed in 0.01518 seconds  
NUM_BLOCK NUM_THREAD 120 80    PI = 3.141592653589789 computed in 0.01614 seconds  
NUM_BLOCK NUM_THREAD 60 160    PI = 3.141592653589789 computed in 0.01617 seconds  
NUM_BLOCK NUM_THREAD 600 16    PI = 3.141592653589789 computed in 0.01679 seconds  
NUM_BLOCK NUM_THREAD 180 48    PI = 3.141592653589793 computed in 0.01723 seconds  
NUM_BLOCK NUM_THREAD 60 144    PI = 3.141592653589793 computed in 0.01794 seconds  
NUM_BLOCK NUM_THREAD 240 32    PI = 3.1415926535898 computed in 0.01884 seconds  
NUM_BLOCK NUM_THREAD 120 64    PI = 3.1415926535898 computed in 0.01888 seconds  
NUM_BLOCK NUM_THREAD 60 128    PI = 3.1415926535898 computed in 0.01938 seconds  
NUM_BLOCK NUM_THREAD 60 112    PI = 3.141592653589795 computed in 0.02212 seconds  
NUM_BLOCK NUM_THREAD 420 16    PI = 3.141592653589795 computed in 0.02216 seconds  
NUM_BLOCK NUM_THREAD 180 32    PI = 3.141592653589796 computed in 0.02467 seconds  
NUM_BLOCK NUM_THREAD 60 96     PI = 3.141592653589796 computed in 0.02508 seconds  
NUM_BLOCK NUM_THREAD 120 48    PI = 3.141592653589796 computed in 0.02513 seconds  
NUM_BLOCK NUM_THREAD 360 16    PI = 3.141592653589796 computed in 0.02572 seconds  
NUM_BLOCK NUM_THREAD 60 80     PI = 3.141592653589793 computed in 0.03006 seconds  
NUM_BLOCK NUM_THREAD 300 16    PI = 3.141592653589793 computed in 0.03018 seconds  
NUM_BLOCK NUM_THREAD 60 64     PI = 3.141592653589792 computed in 0.03689 seconds  
NUM_BLOCK NUM_THREAD 120 32    PI = 3.141592653589792 computed in 0.03694 seconds  
NUM_BLOCK NUM_THREAD 240 16    PI = 3.141592653589792 computed in 0.03751 seconds  
NUM_BLOCK NUM_THREAD 60 48     PI = 3.141592653589794 computed in 0.04916 seconds  
NUM_BLOCK NUM_THREAD 180 16    PI = 3.141592653589794 computed in 0.0492 seconds  
NUM_BLOCK NUM_THREAD 60 32     PI = 3.141592653589793 computed in 0.07178 seconds  
NUM_BLOCK NUM_THREAD 120 16    PI = 3.141592653589793 computed in 0.07376 seconds  
NUM_BLOCK NUM_THREAD 60 16     PI = 3.141592653589794 computed in 0.1555 seconds   

