#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <limits.h>

unsigned long long fact(int, int);

int main() {
    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes and rank of the process
    int size, my_rank, chunk;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    int i, count;
    double e, pe, euler;
    int begin,end,r;
    int chunks[2];
    int rchunks[2];
    int tag = 5;


    count = 1000000000;
    chunk = count/(size-1);

    if (my_rank == 0) {
      begin = 0;
      for (r=1;r<size;r++) {
	
	end = begin + chunk-1;
	if (r == size-1) {
          end = count;
	}

        chunks[0] = begin; 
        chunks[1] = end; 
        MPI_Send(&chunks,2,MPI_INTEGER,r,tag,MPI_COMM_WORLD);
        begin = r*chunk;

        MPI_Recv(&e,1,MPI_DOUBLE,r,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
        /*printf("Received from rank %d e = %.30f\n", r, e);*/
	euler += e;
      }

      printf("Euler number = %.30f\n", euler);

    } else {
      MPI_Recv(&rchunks,2,MPI_INTEGER,0,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

      pe = 1.0/fact(rchunks[0], my_rank);
      for (i=rchunks[0]+1;i<=rchunks[1];i++) {
	pe += 1.0/fact(i, my_rank);	
      }

      /*printf("Sending pe = %f from rank %d\n", pe, my_rank);*/
      MPI_Send(&pe,1,MPI_DOUBLE,0,tag,MPI_COMM_WORLD);

    }  


    MPI_Finalize();

    return(0);
}

unsigned long long fact(int n, int my_rank) {
  int c;
  unsigned long long r = 1;

  for (c = 1; c <= n; c++) {
    if (r > ULLONG_MAX / c)
       break;
    r = r * c;
  }
  return r;
}
