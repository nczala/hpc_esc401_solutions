#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {
    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes and rank of the process
    int size, my_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Request send_requests[size];
    MPI_Request recv_requests[size];
    MPI_Request requests[size];
    MPI_Status status;

    int my_sum = 0;
    int i;
    int tag = 5;
    int recvready;

    int send_rank = my_rank;  // Send    buffer
    int recv_rank = 0;        // Receive buffer

    // Compute the ranks of left/right neighbours 
    int left_rank, right_rank;
    left_rank = my_rank -1;
    if (left_rank < 0) {
      left_rank = size - 1;
    }
    right_rank = my_rank + 1;
    if (right_rank >= size) {
      right_rank = 0;
    } 

    // Loop over the number of processes
    for (i = 0; i < size; ++i) {
      //     send to right, receive from left
      MPI_Irecv(&recv_rank,1,MPI_INTEGER,left_rank,tag,MPI_COMM_WORLD,&requests[0]);
      MPI_Isend(&send_rank,1,MPI_INTEGER,right_rank,tag,MPI_COMM_WORLD,&requests[1]);

      MPI_Waitall(2, requests, MPI_STATUS_IGNORE);

      //     update the send buffer
      send_rank = recv_rank;
      //     update the local sum
      my_sum += recv_rank;
    }



    printf("I am processor %d out of %d, and the sum is %d\n", my_rank, size, my_sum);

    // Finalize the MPI environment.
    MPI_Finalize();
}
