# Exercise Session 6 #

**Exercise 1**

all MPI processes were compiled like this:  
``` cc -O3 -o programm programm.c```  

1. 
look at the code ```ring_deadlock.c``` and it's output ```slurm-30759949.out```  
When using synchronous blocking MPI_Send and MPI_Reccv for all processes, it leads to a classic **deadlock**.  
Which means that all processors wait for the matching receive, so nothing happens, and the job get canceled.  

  
2.  
look at the code ```ring_no_deadlock.c``` and it's output ```slurm-30760579.out```  
When dividing the processes in 2 groups, the problem is overcome.  
This works, because **never** all processes at the same time, the even processes and odd processes send and receive alternately.  
  

3.  
look at the code ```ring_mpi_waitall.c``` and it's output ```slurm-30760126.out```
This solution works better than synchronous communication, because it uses non-blocking communication.  
This means the processes can complete without waiting fot the communication. So this solution works faster!


**Exercise 2**  
look at the code ```euler.c``` and it's output ```slurm-30761365.out```
The programm approximated *e* to **2.718281828459049975776906649116**.

**Exercise 3**  
I wasn't able to solve this exercise.  