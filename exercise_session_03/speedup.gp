set title 'OpenMP Speedup'
set xlabel 'Threads'
set ylabel 'Speedup'
set key top left
set term png
set output "speedup.png"
plot x lc 2 lw 2 t 'Ideal Speedup’,\
"out.dat" u 1:(1.151660/$2) w lp lc 1 lw 2
