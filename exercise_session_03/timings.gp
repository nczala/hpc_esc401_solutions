set title 'OpenMP Timing'
set xlabel 'Threads'
set ylabel 'Time (seconds)'
set key top right
set term png
set output "timings.png"
plot "out.dat" u 1:2 w lp lw 2 t 'OpenMP'
