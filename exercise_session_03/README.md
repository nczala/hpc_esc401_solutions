#Exercise Sheet  3 #

**Exercise 1**

The first value is the execution time for the particular rank. tWElapsed()  
The second value is the total execution time of the whole programm. getTime()  

**Exercise 2**

Results of different optimisation flags:  
-O0:sum=10022429073.341873, Execution Time: 6.401 seconds  
-O1:sum=10022429073.341873, Execution Time: 2.224 seconds  
-O2:sum=10022429073.341873, Execution Time: 2.053 seconds  
-O3:sum=10022429073.341873, Execution Time: 2.049 seconds  

Other ways to get timings:  
- time ./sum  
- omp_get_wtime()  
- MPI_Wtime()  

Difference in execution time with different flags:  
The biggest performence gain is between flag -O0 and -O1.  

How did the compiler optimize the code?  
The command 'man cc 1+2 /-O' shows all optimization flags applied for the value 0,1,2,3.  

How can you make it even faster?  
CFLAGS=-O3 -ffast-math -mavx2  
With this flag, the execution time is reduced to: 2.011 seconds  

Parallelization:  
The line was inserted directly before the for-loop and was compiled with the -fopenmp flag.  

Because of an accounting issue I tried to run this exercise on my local computer node.  
I plotted the the timing versus node, this plot can be found in the file timings.png in my repository.  
As it can be seen the execution time first dropped first rapidly and then flattend out for a higher number of threads.  
This represents the theory we learned in  class perfectly.    

The speedup plot can be found in the file speedup.png. But unfourtunately I wasn't able to plot it correctly.  

**Exercise 3**

Superlinear speedup can come from: cache usage, memory hit patterns or improved algorithms.  
 
