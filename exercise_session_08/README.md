# Exercise Sheet 8 #

**Exercise 1**

list with updated packages, look at file: ```package_list_nzala_ex8```  
visualization of the output files, look at file: ```test.png```  

**Exercise 2**

To access the output files of the container, I linked the host directory to the container with the following command:   
```sudo docker run -v /home/ubuntu/hpc_esc_401/exercise_session_08/poisson_solver/output2:/poisson/output poisson:lates```  

