# Exercise Session 02 #  
**Exercise 1**  
Problems: initial origin pointed to github and not bitbucket  

Solutions:  
-initialized new repo on bitbucket  
-switched remote origin from github to bitbucket  
-merged github master into bitbucket master  
-commited and pushed merge to bitbucked  

**Exercise 2**   
cc: gnu C and C++ compiler  

-O3: optimization level 3, more addressive than -O2 or -O1  

-O0: default optimization level  

Programming environment: cray/6.0.8  

**Exercise 3**  
finding users jobs: squeue -u username  
sinfo -p debug: informations on partition debug  

redirecting output and error:  
#SBATCH -e errors.log  
#SBATCH -o output.log  
  
BONUS:  squeue -o "%.18i %.u %.p %.a %.j %.D %.t %.r %.S %.L %.p"

**Exercise 4**  
see cpi_hybrid.c