#! /bin/bash

number=$1

# check if command line argument is empty or not present
if [ $# == 0 ]; then
	echo -n "Please insert a valide input:"
	read number
fi

# check if input is a number
regex="^[0-9]+$"
if [[ ! $number =~ $regex ]];then
 	echo "Input is not a number"
	exit 1		
fi

# initialize flag file
p=0

function isprime () {
	local x=$number
	i=2
	while [ $i -le `expr $x / 2` ];do
		if [ `expr $x % $i` -eq 0 ]; then
		 	p=1	
                        break
		fi
		i=$((i+1))
	done
}

# execute function isprime with argument $number
isprime $number

if [ $p -eq 0 ]; then
	echo "$number is prime"
else
	echo "$number is not prime"
fi

exit 0
