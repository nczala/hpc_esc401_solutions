#! /bin/bash

begin=$1
end=$2

function usage () {
  echo "Please insert begin and end of range"
  exit 1
}

# check if command line argument is empty or not present
if [ $# != 2 ]; then
	usage
fi

# check if input are numbers
regex="^[0-9]+$"

for number in $begin $end; do
	if [[ ! $number =~ $regex ]];then
 		echo "Input is not a number"
		exit 2		
	fi
done 

function isprime () {
        p=0
	local x=$j
	i=2
	while [ $i -le `expr $x / 2` ];do
		if [ `expr $x % $i` -eq 0 ]; then
		 	p=1	
                        break
		fi
		i=$((i+1))
	done
}

# execute function isprime with argument $number
j=$begin
while [ $j -le $end ]; do
  isprime
  if [ $p -eq 0 ]; then
	echo "$j"
  fi
  j=$((j+1))
done

exit 0
