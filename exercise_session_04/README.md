# Exercise Sheet 4

**Exercise 1**  

*File* permissions:  
```$HOME```: drwx------ or 700  
```$SCRATCH```: drwxr-xr-x or 755  

Access to *other student's* ```$HOME``` or ```$SCRATCH```:  
Most ```$HOME``` directories are only accessible by the owner, but some are freely accessible but not writable.  
See command: ```ls -ld /users/*``` 

Nearly all ```$SCRATCH``` directories are accessible for all, some users even have 777 permission!  
See command: ```ls -ld /scratch/snx3000/*```  

*Default* permissions:  
```$HOME```: rw-r--r-- or 644  
```$SCRATCH```: rw-r--r-- or 644  

Permission of ```/users/hlascomb```:  
```$HOME```: rwxr-x---+ or 750  
```$SCRATCH```: rwxr-xr-x or 755   

If there was a file ```/users/hlascomb/hpc_exam_solution.txt``` I would not be able to open it, nor to write it, because we are not in the same group.    
But if the file was in the ```$SCRATCH``` directory I would be able to read and execute it, if the file has the same permission as the ```$SCRATCH``` directory.  

```chmod 700 file``` or ```chmod u+rwx,g-rwx,o-rwx file```  
These commands would set the access permission of a directory only to the owner.  

```touch file.txt; chmod 760 file.txt```  
This command sets the access permission that group members can write to it.  

```touch file.txt; chmod 000 file.txt```
With this command, the file can neither be opened nor written or executed.  
But the file is not completely lost, because we can change its permission again.   

**Exercise 2**

look at the following scripts: ```double_spacing.sh```, ```tripple_spacing.sh```, ```single_spacing.sh```  

**Exercise 3**

look at the following script: ```is_prime.sh```   
423083 is a prime number, the programm needed 72 minutes to execute the code, which means the performance of the code can still be massively improved.  
4572862171001 is also a prime number.   

_**Bonus**_: look at the script: ```is_prime_bonus.sh```  