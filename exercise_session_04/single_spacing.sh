#! /bin/bash
filename=$1
#check if command line argument is empty or not present
if [ $# == 0 ]; then
	echo "No file given!"
	exit 1
elif [ ! -f "$1" ]; then
	echo "$1 does not exist."
	exit 2
elif [ ! -s "$1" ]; then
	echo "File exists but is empty"
	exit 3
fi

while read line; do
	if [ ! -z "$line" ];then   
		echo "$line"
	fi
done < $filename

exit 0
