#ifndef _PARTICLE_H
#define _PARTICLE_H

#include <vector>
struct particle {
	float x, y, z; // position 
	float vx, vy, vz; // velocity 
	float ax, ay, az; // acceleration
};
typedef std::vector<particle> particles;

#endif
