#include "forces.h"
#include "ic.h"
#include <iostream>
#include <sstream> // for std::stringstream
#include <time.h>
#include <chrono>

int main(int argc, char *argv[]) {

  int N;

  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

  if (argc < 2) {
    std::cerr << "Usage: " << argv[0] << " <Number of particles>" << std::endl;
    return 1;
  }

  std::stringstream convert{ argv[1] };

  if (!(convert >> N)) {
    std::cerr << "Input is not a number!" << std::endl;
    return 2;
  }

	particles plist; // vector of particles
	ic(plist,N); // initialize starting position/velocity 
	forces(plist); // calculate the forces

  
  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
  printf("particles: %d  time: %ld ms\n", N, std::chrono::duration_cast<std::chrono::milliseconds> (end - begin).count());

	return 0;
}

