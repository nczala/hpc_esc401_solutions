# Exercise Session 05 #


**Exercise 1**

1)end with 00  
```grep '00$' binary.txt```  
```egrep'[01]+00$' binary.txt```  

2)start and end with 1  
```grep '^1.\*1$' binary.txt```  
```grep '^1[01]\*1$' binary.txt```  
```egrep '^1[01]+1$' binary.txt```  

3)contain pattern 110  
```grep 110 binary.txt```  

4)contain at least 3 times a 1  
```egrep '(1[0-1]\*){3,}' binary.txt``` 
```grep '1[0-1]\*1[0-1]\*1[0-1]\*' binary.txt```  

5) contain at least three consecutive 1s  
```egrep '1{3,}' binary.txt```  

look at the file ```measurements.sh``` and ```out.txt```  

**Exercise 2**

After compiling ```nbody_complete.cpp``` with ```-Og```, there was a WARNING and INFO in the terminal.  

The submition of the job, produced the folder: ```nbody_complete+15190-2357s``` which contains the following folders: ```ap2-files```,```index.ap2```, ```rpt-files```and ```xf-files```.  
These folders contain different ouptut files when using Perftool-lite. 

The part ```forces```uses the most part of CPU time: 83.3%. Followed by ```vector operation```which uses 16.2%.  

The log file is structures like this:  
-first general information, execution time, memory usage etc.  
-profiling information of the code  
-memory bandwidth  
-energy and power usage  


**Extraction in seperate modules**  

The code is more readable if the particles is extracted in a seperate file: ```particles.h```, but it works either way.  

The CPU usage does **NOT** differ much to the original .cpp file. THere is **NO** difference in runtime, the extraction has **NO** influence on the performance of the programm.  
The execution time is in both cases around 13 seconds.  

**Exercise 3**  

All the files and codes to this exercise can be found in the directory: ```exercise_session_05/ÒpenMP```  

Parallelizing the **first loop** is more efficient than the second. Parallelizing both doesn't change the execution time.  
Due to this, I think parallelizing the first loop automatically also parallelizes the nested loop.  

I would expect that the execution time **t** scales **quadratically** with the number of paricles **N**, because of the 2 loops which loop both over n.  
```n*n = n^2```

The plot of the execution time can be found in this file: **```timings.png```**  
The job which uses 12 threads has a much better execution time than the job using only 1 thread.  






