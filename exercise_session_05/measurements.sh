#!/bin/bash

inputFile="measurements.txt"
regexp="Temperature: ([0-9]+\.[0-9]+) deg at time: ([0-9]+\.[0-9]+) sec"

totalTime=0
echo "Temperature	Time"
while read line ; do
if [[ "$line" =~ $regexp ]] ; then
echo "${BASH_REMATCH[1]}	${BASH_REMATCH[2]}"
totalTime=$(echo "scale=5;$totalTime +\
${BASH_REMATCH[2]}" | bc)
fi
done<"$inputFile"
echo "Total time: $totalTime seconds"
